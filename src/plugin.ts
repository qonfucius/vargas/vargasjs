import { ICEntity } from './models/entity';

export interface IPlugin {
    install(entity: ICEntity): void;
}
