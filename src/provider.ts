import { IPlugin } from './plugin';
import Fulfillable from './thenables/fulfillable';
import { ICEntity, IEntity } from './models/entity';

export interface IProvider extends IPlugin {
    isConnected(): Promise<boolean>;
    connect(): Promise<void>;
    save(entity: IEntity): Fulfillable<IEntity>;
    prepare(entities: { [key: string]: ICEntity }): Promise<void>;
}
