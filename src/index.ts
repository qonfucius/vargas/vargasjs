import Store, { IStore } from './store';
import { IProvider } from './provider';
import * as Models from './models/index';
import Entity from './models/entity';

export { Models };

export const DEFAULT_NAMESPACE = 'default';

export interface IVargas {
    store: {
        [key: string]: IStore;
    };
    isConnected(namespace: string): Promise<boolean>;
    connect(provider: IProvider, namespace?: string): Promise<void>;
    register(model: typeof Entity, name?: string, namespace?: string): typeof Entity;
    prepare(namespace?: string): Promise<void>;
}

export class Vargas implements IVargas {
    store: {
        [key: string]: IStore;
    };

    constructor() {
      this.store = {};
    }

    private getStore(namespace = DEFAULT_NAMESPACE): IStore {
      if (!(namespace in this.store)) {
        this.store[namespace] = new Store();
      }
      return this.store[namespace];
    }

    public async isConnected(namespace = DEFAULT_NAMESPACE): Promise<boolean> {
      return this.getStore(namespace).isConnected();
    }

    public async connect(provider: IProvider, namespace = DEFAULT_NAMESPACE): Promise<void> {
      return this.getStore(namespace).connect(provider);
    }

    register(model: typeof Entity, name?: string, namespace = DEFAULT_NAMESPACE): typeof Entity {
      return this.getStore(namespace).register(model, name);
    }

    prepare(namespace = DEFAULT_NAMESPACE): Promise<void> {
      return this.getStore(namespace).prepare();
    }
}

const defaultInstance = new Vargas();
export const mainVargas = defaultInstance;
export default defaultInstance;
