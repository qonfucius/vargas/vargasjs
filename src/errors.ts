export const NotFoundError = Error;
export const ModelNotFoundError = NotFoundError;

export const ConfigurationError = Error;
export const MissingConfigurationError = ConfigurationError;
