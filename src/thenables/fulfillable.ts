import { IEntity } from '../models/entity';

// @todo: move in provider
interface IWithNew {
    new?: {};
}
type Reason = string|void|number|boolean|{};
export default class Fulfillable<T> extends Promise<T> {
    private entity?: IEntity;

    constructor(
      executor: (
            resolve: (value?: T | PromiseLike<T>) => void,
            reject: (reason?: Reason) => void
        ) => void, entity?: IEntity,
    ) {
      super(executor);
      this.entity = entity;
    }

    static fromPromise<T>(promise: Promise<T>, entity?: IEntity): Fulfillable<T> {
      return new Fulfillable<T>((resolve, reject): void => { promise.then(resolve, reject); }, entity);
    }

    fulfill(): Promise<void|T|IEntity> {
      return this.then((data: IWithNew) => this.entity && this.entity.setRaw(data.new || {}));
    }
}
