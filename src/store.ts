import { IProvider } from './provider';
import { ModelNotFoundError, MissingConfigurationError } from './errors';
import Entity from './models/entity';

export interface IStore {
    provider?: IProvider;
    models: {
        [key: string]: typeof Entity;
    };
    isConnected(): Promise<boolean>;
    connect(provider: IProvider): Promise<void>;
    register(model: typeof Entity, name?: string): typeof Entity;
    prepare(): Promise<void>;
    get(name: string|typeof Entity): typeof Entity;
}

export default class Store implements IStore {
    provider?: IProvider;

    models: {
        [key: string]: typeof Entity;
    };

    constructor() {
      this.models = {};
    }

    public async isConnected(): Promise<boolean> {
      return !!this.provider && this.provider.isConnected();
    }

    public async connect(provider: IProvider): Promise<void> {
      this.provider = provider;
      await this.provider.connect();
    }

    register(model: typeof Entity, name?: string): typeof Entity {
      const n = name || model.name;
      if (n.length < 1) {
        throw new MissingConfigurationError('Model name cannot be empty');
      }
      model.configure(this);
      this.models[n] = model;

      return model;
    }

    get(name: string|typeof Entity): typeof Entity {
      const n = typeof name === 'string' ? name : name.name;
      const model = this.models[n];
      if (!model) {
        throw new ModelNotFoundError('Model not found');
      }
      return model;
    }

    async prepare(): Promise<void> {
      return this.provider && this.provider.prepare(this.models);
    }
}
