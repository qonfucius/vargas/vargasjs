import Entity from './entity';

export const DOCUMENT = 'document';

export default abstract class Document extends Entity {
  static get type(): string { return DOCUMENT; }
}
