import Document from './document';

export const EDGE = 'edge';

export default abstract class Edge extends Document {
  static get type(): string { return EDGE; }
}
