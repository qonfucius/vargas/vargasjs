import Document from './document';
import Edge from './edge';

export { Document, Edge };
export type Model = Document | Edge;
