import { IStore } from '../store';
import Fulfillable from '../thenables/fulfillable';

type RawObject = {};

export const Entity: ICEntity = class Entity implements IEntity {
    static store?: IStore;

    rawData = {};

    static type: string;

    constructor(data: RawObject = {}) {
      this.setRaw(data);
    }

    static configure(store: IStore): void {
      this.store = store;
    }

    save(): Fulfillable<IEntity> {
      const model = this.constructor as ICEntity;
      if (!model.store || !model.store.provider) {
        throw new Error('Model not registered or no provider set');
      }
      return model.store.provider.save(this);
    }

    getRaw(): RawObject {
      return this.rawData;
    }

    setRaw(data: RawObject): IEntity {
      this.rawData = data;
      return this;
    }

    assignData(...data: RawObject[]): IEntity {
      Object.assign(this.rawData, ...data);
      return this;
    }
};

export default Entity;

export interface IEntity {
    save(): Fulfillable<IEntity>;
    getRaw(): RawObject;
    setRaw(data: RawObject): IEntity;
    assignData(...data: RawObject[]): IEntity;
}

export interface ICEntity {
    new (data?: RawObject): IEntity;
    store?: IStore;
    name: string;
    collectionName?: string;
    configure(store: IStore): void;
    type: string;
}
