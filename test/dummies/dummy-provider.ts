import { IProvider } from '../../src/provider';
import Entity, { IEntity } from '../../src/models/entity';
import Fulfillable from '../../src/thenables/fulfillable';

export default class DummyProvider implements IProvider {
    /* eslint-disable class-methods-use-this,@typescript-eslint/no-empty-function */
    private connected = false;

    async isConnected(): Promise<boolean> {
      return this.connected;
    }

    async connect(): Promise<void> {
      this.connected = true;
    }

    install(): void {}

    async prepare(): Promise<void> {
    }

    save(): Fulfillable<IEntity> {
      return Fulfillable.fromPromise<IEntity>(Promise.resolve(new Entity()));
    }
}
