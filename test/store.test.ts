import Store from '../src/store';
import DummyProvider from './dummies/dummy-provider';
import DummyEntity from './dummies/dummy-entity';

describe('Test main class', () => {
  it('Should be able to connect via uri', async () => {
    const store = new Store();
    // store.register(DummyEntity, '');
    expect(await store.isConnected()).toBeFalsy();
    await store.connect(new DummyProvider());
    await store.prepare();
    expect(await store.isConnected()).toBeTruthy();
  });
  it('Should be able to register model', async () => {
    const store = new Store();
    store.register(DummyEntity);
    expect(() => {
      store.register(class Foo extends DummyEntity {
        // @ts-ignore
        static get name(): string { return ''; }
      });
    }).toThrow();
    store.get(DummyEntity);
    expect(() => {
      store.get('foo');
    }).toThrow();
  });
});
