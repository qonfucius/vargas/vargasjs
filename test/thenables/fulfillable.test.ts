import DummyEntity from '../dummies/dummy-entity';
import Fulfillable from '../../src/thenables/fulfillable';

describe('Test fulfillable class', () => {
  it('Fulfill be able to return Promise', async () => {
    const rawData = { foo: true };
    const entity = new DummyEntity();
    const fulfillable = Fulfillable.fromPromise(Promise.resolve({ new: rawData }), entity);
    expect(fulfillable).toBeInstanceOf(Promise);
    const entityPromise = fulfillable.fulfill();
    expect(entityPromise).toBeInstanceOf(Promise);
    expect(await entityPromise).toEqual(entity);
    expect(entity.getRaw()).toEqual(expect.objectContaining(rawData));
  });
  it('Fulfill be able to return Promise without new data', async () => {
    const entity = new DummyEntity();
    await Fulfillable.fromPromise(Promise.resolve({}), entity).fulfill();
    expect(entity.getRaw()).toEqual({});
  });
});
