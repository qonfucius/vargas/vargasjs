import mainVargas, { Vargas } from '../src/index';
import Store from '../src/store';
import DummyProvider from './dummies/dummy-provider';
import DummyEntity from './dummies/dummy-entity';

const NAMESPACE_KEY = 'foo';

describe('Test Vargas class', () => {
  it('Default export should be be a Vargas instance', () => {
    expect(mainVargas).toBeInstanceOf(Vargas);
  });

  it('Should be instanciable with custom namespace key', async () => {
    const vargas = new Vargas();
    vargas.register(DummyEntity, 'dummy-entity', NAMESPACE_KEY);
    expect(await vargas.isConnected(NAMESPACE_KEY)).toBeFalsy();
    {
      // @ts-ignore: Test private method
      expect(vargas.getStore(NAMESPACE_KEY)).toBeInstanceOf(Store);
    }
    await vargas.connect(new DummyProvider(), NAMESPACE_KEY);
    await vargas.prepare(NAMESPACE_KEY);
    expect(await vargas.isConnected(NAMESPACE_KEY)).toBeTruthy();
  });

  it('Should be instanciable with default namespace key', async () => {
    const vargas = new Vargas();
    vargas.register(DummyEntity, 'dummy');
    vargas.register(DummyEntity);

    expect(await vargas.isConnected()).toBeFalsy();
    {
      // @ts-ignore: Test private method
      expect(vargas.getStore()).toBeInstanceOf(Store);
    }
    await vargas.connect(new DummyProvider());
    await vargas.prepare();
    expect(await vargas.isConnected()).toBeTruthy();
  });
});
