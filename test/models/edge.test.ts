import Edge, { EDGE } from '../../src/models/edge';

describe('Test edge class', () => {
  it('Test Type', async () => {
    expect(Edge.type).toEqual(EDGE);
  });
});
