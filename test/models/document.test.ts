import Document, { DOCUMENT } from '../../src/models/document';

describe('Test document class', () => {
  it('Test Type', async () => {
    expect(Document.type).toEqual(DOCUMENT);
  });
});
