import Entity from '../../src/models/entity';
import { Vargas } from '../../src';
import DummyProvider from '../dummies/dummy-provider';

describe('Test entity class', () => {
  it('Test save', async () => {
    const entity = new Entity();
    expect(() => entity.save()).toThrow();
    const vargas = new Vargas();
    vargas.connect(new DummyProvider());
    vargas.register(Entity);
    await expect(entity.save()).resolves.toBeInstanceOf(Entity);
  });
  it('Test raw manipulation', () => {
    const entity = new Entity();
    const fooObject = { foo: true };
    entity.setRaw(fooObject);
    expect(entity.getRaw()).toEqual(expect.objectContaining(fooObject));
    const barObject = { bar: true };
    entity.assignData(barObject);
    expect(entity.getRaw()).toEqual(expect.objectContaining(fooObject));
    expect(entity.getRaw()).toEqual(expect.objectContaining(barObject));
  });
});
